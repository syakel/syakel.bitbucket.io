var annotated_dup =
[
    [ "BNO055", null, [
      [ "BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "controller", null, [
      [ "ClosedLoop", "classcontroller_1_1ClosedLoop.html", "classcontroller_1_1ClosedLoop" ]
    ] ],
    [ "controller_fsfb", null, [
      [ "ClosedLoop", "classcontroller__fsfb_1_1ClosedLoop.html", "classcontroller__fsfb_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847", null, [
      [ "DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor_driver", null, [
      [ "Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ],
      [ "Motor_Driver", "classmotor__driver_1_1Motor__Driver.html", "classmotor__driver_1_1Motor__Driver" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_control", null, [
      [ "Task_Control", "classtask__control_1_1Task__Control.html", "classtask__control_1_1Task__Control" ]
    ] ],
    [ "task_controller", null, [
      [ "Task_Controller", "classtask__controller_1_1Task__Controller.html", "classtask__controller_1_1Task__Controller" ]
    ] ],
    [ "task_data_collect", null, [
      [ "Task_Data_Collect", "classtask__data__collect_1_1Task__Data__Collect.html", "classtask__data__collect_1_1Task__Data__Collect" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_IMU", null, [
      [ "Task_IMU", "classtask__IMU_1_1Task__IMU.html", "classtask__IMU_1_1Task__IMU" ]
    ] ],
    [ "task_touch", null, [
      [ "Task_Touch", "classtask__touch_1_1Task__Touch.html", "classtask__touch_1_1Task__Touch" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "task_user_old", null, [
      [ "Task_User", "classtask__user__old_1_1Task__User.html", "classtask__user__old_1_1Task__User" ]
    ] ],
    [ "touch_panel", null, [
      [ "Touch_Panel", "classtouch__panel_1_1Touch__Panel.html", "classtouch__panel_1_1Touch__Panel" ]
    ] ]
];