var classBNO055_1_1BNO055 =
[
    [ "__init__", "classBNO055_1_1BNO055.html#af4536335d1ab9fd46b11c3c3e315d49e", null ],
    [ "change_opMode", "classBNO055_1_1BNO055.html#acae74cda7f9da87803f2534c5b42e83b", null ],
    [ "get_calibrationCoeff", "classBNO055_1_1BNO055.html#a86972be7c54a0257318643551a1b3604", null ],
    [ "get_calibrationStatus", "classBNO055_1_1BNO055.html#a61581576cacad1259bdeaa3f0da3cd74", null ],
    [ "read_angularVelocity", "classBNO055_1_1BNO055.html#ab0796ddc97e39ae2076be47b7b6c2cc6", null ],
    [ "read_eulerAngles", "classBNO055_1_1BNO055.html#a6739cd3e8e007336b3c521f5168b7dbc", null ],
    [ "write_calibrationCoeff", "classBNO055_1_1BNO055.html#a62a0056c24f81dff3c780edb8071a1ca", null ]
];