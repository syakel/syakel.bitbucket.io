var files_dup =
[
    [ "BNO055.py", "BNO055_8py.html", [
      [ "BNO055.BNO055", "classBNO055_1_1BNO055.html", "classBNO055_1_1BNO055" ]
    ] ],
    [ "controller.py", "controller_8py.html", [
      [ "controller.ClosedLoop", "classcontroller_1_1ClosedLoop.html", "classcontroller_1_1ClosedLoop" ]
    ] ],
    [ "controller_fsfb.py", "controller__fsfb_8py.html", [
      [ "controller_fsfb.ClosedLoop", "classcontroller__fsfb_1_1ClosedLoop.html", "classcontroller__fsfb_1_1ClosedLoop" ]
    ] ],
    [ "DRV8847.py", "DRV8847_8py.html", [
      [ "DRV8847.DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847.Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "main_old.py", "main__old_8py.html", "main__old_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor_driver.py", "motor__driver_8py.html", [
      [ "motor_driver.Motor_Driver", "classmotor__driver_1_1Motor__Driver.html", "classmotor__driver_1_1Motor__Driver" ],
      [ "motor_driver.Motor", "classmotor__driver_1_1Motor.html", "classmotor__driver_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_control.py", "task__control_8py.html", "task__control_8py" ],
    [ "task_controller.py", "task__controller_8py.html", "task__controller_8py" ],
    [ "task_data_collect.py", "task__data__collect_8py.html", "task__data__collect_8py" ],
    [ "task_encoder.py", "task__encoder_8py.html", "task__encoder_8py" ],
    [ "task_IMU.py", "task__IMU_8py.html", "task__IMU_8py" ],
    [ "task_touch.py", "task__touch_8py.html", "task__touch_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "task_user_old.py", "task__user__old_8py.html", "task__user__old_8py" ],
    [ "touch_panel.py", "touch__panel_8py.html", [
      [ "touch_panel.Touch_Panel", "classtouch__panel_1_1Touch__Panel.html", "classtouch__panel_1_1Touch__Panel" ]
    ] ]
];